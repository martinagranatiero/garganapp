package it.uniba.minimal.garganapp;

import java.util.Date;

public class Eventi {
    String titolo;
    String citta;
    String prezzo;
    String data;
    String telefono;
    String indirizzo;
    String email;
    String link;
    String image;
    String userID;
    String cid;
    String descrizione;


    public Eventi(){

    }

    public Eventi(String titolo, String citta, String prezzo, String data,
                  String telefono, String indirizzo, String email,
                  String link, String image, String userID, String cid) {
        this.titolo = titolo;
        this.citta = citta;
        this.prezzo = prezzo;
        this.data = data;
        this.telefono = telefono;
        this.indirizzo = indirizzo;
        this.email = email;
        this.link = link;
        this.image = image;
        this.userID = userID;
        this.cid = cid;
    }

    public String getTitolo() {
        return titolo;
    }

    public String getCitta() {
        return citta;
    }

    public String getPrezzo() {
        return prezzo;
    }

    public String getData() {
        return data;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public String getEmail() {
        return email;
    }

    public String getLink() {
        return link;
    }

    public String getImage() {
        return image;
    }

    public String getUserID() {
        return userID;
    }

    public String getCid() {
        return cid;
    }

    public String getDescrizione() {  return descrizione; }

    public void setDescrizione(String descrizione) {  this.descrizione = descrizione;}
}
