package it.uniba.minimal.garganapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

public class DashboardAreaStaff extends AppCompatActivity {

    Toolbar toolbar;
    CardView aggiungiCitta;
    CardView elencoCitta;
    CardView logout;
    CardView elencoUtenti;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_area_staff);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.menu_toolbar_title);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        fAuth=FirebaseAuth.getInstance();

            aggiungiCitta=findViewById(R.id.cardViewAggiungiCitta);
            elencoCitta = findViewById(R.id.cardViewElencoCitta);
            logout = findViewById(R.id.cardViewLogout);
            elencoUtenti = findViewById(R.id.cardViewElencoUtenti);


            aggiungiCitta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(DashboardAreaStaff.this,AggiungiCittaActivity.class));
                }
            });

            elencoCitta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(DashboardAreaStaff.this, ElencoCittaActivity.class));
                }
            });

            elencoUtenti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(DashboardAreaStaff.this, ElencoUtentiActivity.class));
                }
            });

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fAuth.getInstance().signOut();
                    Toast.makeText(DashboardAreaStaff.this,R.string.logout,Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                }
            });

    }


    //Presenza alertDialog per poter permettere all'utente la conferma di voler effettuare il logout o meno
    public void onBackPressed() {
        final AlertDialog.Builder logoutDialog = new AlertDialog.Builder(this);
        logoutDialog.setTitle(getString(R.string.confirm_exit));
        logoutDialog.setIcon((R.drawable.logout));
        logoutDialog.setMessage(getString(R.string.exit_confirm));
        logoutDialog.setCancelable(false);
        logoutDialog.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fAuth.getInstance().signOut();
                Toast.makeText(DashboardAreaStaff.this,R.string.logout,Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });
        logoutDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog= logoutDialog.create();
        alertDialog.show();
    }

    //Per ragioni di sicurezza, ad accesso effettuato, ogni volta che il membro dello staff termina l'app
    //viene effettuato il logout
    @Override
    protected void onStop() {
        super.onStop();
        fAuth.getInstance().signOut();
    }
}
