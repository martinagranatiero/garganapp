package it.uniba.minimal.garganapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG_LOG = SplashActivity.class.getName();

    //intervallo minimo da attendere per passare all'attività successiva
    private static final long MIN_WAIT_INTERVAL = 1000L;
    //intervallo dopo il quale passerà in automatico
    private static final long MAX_WAIT_INTERVAL = 1500L;
    private static final int GO_AHEAD_WHAT = 1;

    private long mStartTime;
    private boolean mIsDone;

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage (Message msg){
            switch (msg.what){
                case GO_AHEAD_WHAT:
                    long elapsedTime = SystemClock.uptimeMillis()- mStartTime;
                    if(elapsedTime >= MIN_WAIT_INTERVAL && !mIsDone){
                        mIsDone = true;
                        goAhead();
                    }
                    break;
            }
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //Nasconde pulsanti software telefono
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    protected void onStart(){
        super.onStart();
        mStartTime = SystemClock.uptimeMillis();
        final Message goAheadMessage = mHandler.obtainMessage(GO_AHEAD_WHAT);
        mHandler.sendMessageAtTime(goAheadMessage,mStartTime + MAX_WAIT_INTERVAL);
    }

    //Metodo che fa partire un intent esplicito per la main activity
    private void goAhead(){
        final Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        //Utile per eliminare la splash activity dallo stack, in quanto non deve essere possibile tornare indietro dalla home.
        finish();
    }
}
