package it.uniba.minimal.garganapp;

import com.google.firebase.database.Exclude;

import java.util.Comparator;

public class Citta {
     String titolo;
     String image;
     String description;
     String cid;

    public String getDescrizione() {
        return description;
    }

    public void setDescrizione(String descrizione) {
        this.description = descrizione;
    }

    //costruttore
    public Citta() {
    }

    //costruttore
    public Citta(String titolo,String descrizione,String image)
    {
        this.titolo=titolo;
        this.description=descrizione;
        this.image=image;
    }

    //costruttore
    public Citta(String titolo,String mImageUrl)
    {
        this.titolo=titolo;
        this.image=mImageUrl;
    }


    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }


    public String getCid() { return cid; }

    public void setCid(String cid) { this.cid = cid; }

    //per ordinare le città in ordine ascendente
    public static final Comparator<Citta> SORT_BY_ASCENDING = new Comparator<Citta>() {
        @Override
        public int compare(Citta o1, Citta o2) {
            return o1.getTitolo().compareTo(o2.getTitolo());
        }
    };

    //per ordinare le città in ordine decrescente
    public static final Comparator<Citta> SORT_BY_DESCENDING = new Comparator<Citta>() {
        @Override
        public int compare(Citta o1, Citta o2) {
            return o2.getTitolo().compareTo(o1.getTitolo());
        }
    };
}
