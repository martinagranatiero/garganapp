package it.uniba.minimal.garganapp;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;



import java.util.List;

//JavaClass
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
private Context mContext;
//conterrà tutte le città
private List<Citta> mCitta;
private onItemClickListener mListener;

//costruttore adapter (città)
public ImageAdapter(Context context, List<Citta> citta)
{
    mContext=context;
    mCitta=citta;

}
    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View v= LayoutInflater.from(mContext).inflate(R.layout.image_item, parent,false);
      return new ImageViewHolder(v);
    }

    //metodo che mette per ogni card immagine e nome delle città presenti nella lista
    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
     Citta cittaCurrent=mCitta.get(position);
     holder.textViewName.setText(cittaCurrent.getTitolo());
     Picasso.get().load(cittaCurrent.getImage()).fit().centerCrop().into(holder.imageView);
    }

    //ritorna il numero di elementi trovati nella lista di città
    @Override
    public int getItemCount() {
        return mCitta.size();
    }

    public class ImageViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {
    //ogni recyclerview dovrà avere un'immagine ed un nome città
        public TextView textViewName;
        public ImageView imageView;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewName=itemView.findViewById(R.id.text_view_name);
            imageView=itemView.findViewById(R.id.image_view_upload);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mListener != null)
            {
                int position=getAdapterPosition();
                if(position != RecyclerView.NO_POSITION)
                {
                    mListener.onItemClick(position);
                }
            }
        }



    }

    public interface onItemClickListener{
        void onItemClick(int position);


    }

    public void setOnItemClickListener(onItemClickListener listener)
    {
      mListener=listener;
    }
}
