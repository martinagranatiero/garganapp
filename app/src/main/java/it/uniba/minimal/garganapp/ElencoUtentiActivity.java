package it.uniba.minimal.garganapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ElencoUtentiActivity extends AppCompatActivity {

   Toolbar toolbar;
   private FirebaseFirestore db = FirebaseFirestore.getInstance();
   private CollectionReference userRef = db.collection("users");
   private TextView textViewData;
   private TextView numeroUtentiTV;
   private int countUtenti = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elenco_utenti);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.toolbar_name_elencoUtenti);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textViewData = findViewById(R.id.text_view_data);
        numeroUtentiTV = findViewById(R.id.numero_utenti);

        userRef.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                String data = "";

                //ciclo per prendere tutti gli utenti presenti nel database
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    Utente utente = documentSnapshot.toObject(Utente.class);

                    String nome = utente.getName();
                    String email = utente.getEmail();
                    String cognome = utente.getSurname();
                    String telefono = utente.getTelefono();

                    data += getResources().getString(R.string.nome) + ": " + nome + "\n"
                            + getResources().getString(R.string.cognome) + ": " + cognome + "\n"
                            + getResources().getString(R.string.email) + ": " + email + "\n"
                            + getResources().getString(R.string.telefono) + ": " + telefono + "\n\n";

                    countUtenti++;
                }
                //uso dei plurals
                numeroUtentiTV.setText(new StringBuilder().append(getResources().getQuantityString(R.plurals.numeroUtenti, countUtenti, countUtenti)).toString());

                //visualizzo i dati presi dal database
                textViewData.setText(data);
            }
        });
    }
}
