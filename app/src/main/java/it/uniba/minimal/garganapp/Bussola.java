package it.uniba.minimal.garganapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Bussola extends AppCompatActivity implements SensorEventListener {

    ImageView bussola_img;
    TextView txt_bussola;
    int valore;
    private SensorManager mSensorManager;
    //dichiarazione dei tre ssensori utilizzati
    private Sensor mRotationV,mAccelerometer,mMagnetometer;
    //flag per dire all'applicazione se il nostro dispositivo supporta questi sensori
    boolean sensor1=false;
    boolean sensor2=false;
    //per salvare i dati dei sensori
    float[] rMat= new float[9];
    float[] orientation= new float[9];
    private float[] mLastAccelerometer= new float[3];
    private float[] mLastMagnetometer= new float[3];
    //flag per stabilire se abbiamo ricevuto dei dati dall'accelerometro e/o dal magnetometro
    private boolean mLastAccelerometerSet=false;
    private boolean mLastMagnetometerSet=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bussola);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.includebussola);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        bussola_img=findViewById(R.id.image_bussola);
        txt_bussola=findViewById(R.id.tv_bussola);

        start();

    }
    //per reperire le informazioni quando cambia lo stato dei sensori
    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType()==Sensor.TYPE_ROTATION_VECTOR){
            SensorManager.getRotationMatrixFromVector(rMat,event.values);
            valore= (int)((Math.toDegrees(SensorManager.getOrientation(rMat,orientation)[0])+360)%360);
        }
        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            System.arraycopy(event.values,0,mLastAccelerometer,0,event.values.length);
            mLastAccelerometerSet=true;
        }else if(event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD){
            System.arraycopy(event.values,0,mLastMagnetometer,0,event.values.length);
            mLastMagnetometerSet=true;
        }
        if(mLastMagnetometerSet && mLastAccelerometerSet){
            SensorManager.getRotationMatrix(rMat,null,mLastAccelerometer,mLastMagnetometer);
            SensorManager.getOrientation(rMat,orientation);
            valore= (int)((Math.toDegrees(SensorManager.getOrientation(rMat,orientation)[0])+360)%360);
        }
        //per arrotondare per intero il nostro valore
        valore=Math.round(valore);
        //per ruotare l'immagine
        bussola_img.setRotation(-valore);
        //per determinare se ci troviamo a nord,sud,est...
        String where="NO";

        if(valore>=350 || valore<=10) where="N";
        if(valore<350 && valore>280) where="NW";
        if(valore<=280 && valore>260) where="W";
        if(valore<=260 && valore>190) where="SW";
        if(valore<=190 && valore>170) where="S";
        if(valore<=170 && valore>100) where="SE";
        if(valore<=100 && valore>80) where="E";
        if(valore<=80 && valore>10) where="NE";

        txt_bussola.setText(valore+ "° " + where);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    //in questo metodo verificheremo che il dispositivo abbia tutti e 3 i sensori.In caso contrario uscirà un messaggio d'errore.
    public void start(){
        if(mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)== null){
            if(mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)== null || mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)== null){
                noSensorAlert();
            }else{
                mAccelerometer=mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                mMagnetometer=mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
                sensor1=mSensorManager.registerListener(this,mAccelerometer,SensorManager.SENSOR_DELAY_UI);
                sensor2=mSensorManager.registerListener(this,mMagnetometer,SensorManager.SENSOR_DELAY_UI);
            }
        }else{
            mRotationV=mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            sensor1=mSensorManager.registerListener(this,mRotationV,SensorManager.SENSOR_DELAY_UI);
        }
    }
    //Nel caso non siano presenti i sensori richiesti
    public void noSensorAlert(){
        AlertDialog.Builder alertDialog= new AlertDialog.Builder(this);
        alertDialog.setMessage(R.string.compass).setCancelable(false).setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }
    //per disattivare i sensori
    public void stop(){
        if(sensor1 && sensor2){
            mSensorManager.unregisterListener(this,mAccelerometer);
            mSensorManager.unregisterListener(this,mMagnetometer);
        }else{
            if(sensor1)
                mSensorManager.unregisterListener(this,mRotationV);
        }
    }

    @Override
    protected void onPause() {

        super.onPause();
        stop();
    }
    @Override
    protected void onResume() {

        super.onResume();
        start();
    }
}