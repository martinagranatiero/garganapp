package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class AggiungiEventoActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btnCaricaImmagine;
    private Button btnAggiungiEvento;
    private ImageView inputProjectImage;
    private static final int GALLERY_PICK = 1;
    private EditText titolo;
    private EditText descrizione;
    private EditText citta;
    private EditText indirizzo;
    private EditText email;
    private EditText link;
    private EditText data;
    private EditText prezzo;
    private EditText telefono;
    private Uri imageUri;
    private String sTitolo,sCitta,sIndirizzo,sEmail,sLink,sImage,saveCurrentDate,saveCurrentTime,randomKey,downloadImageUrl,sDescrione;
    private String sPrezzo;
    private String sData;
    private String sTelefono;
    private StorageReference eventImagesRef;
    private DatabaseReference eventRef;
    private FirebaseAuth fAuth;
    private String userID;
    private static final String TAG = "MINIMAL_PERMISSIONS";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aggiungi_evento);
        fAuth = FirebaseAuth.getInstance();

        //verifica che l'utente sia loggato
        //solo gli utenti registrati a GarganApp potranno aggiungere gli eventi
            if (fAuth.getCurrentUser() != null) {
                userID = fAuth.getCurrentUser().getUid();
                toolbar = (Toolbar) findViewById(R.id.my_toolbar);
                TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
                setSupportActionBar(toolbar);
                mTitle.setText(R.string.addEvents);
                getSupportActionBar().setDisplayShowTitleEnabled(false);

                //serve per tornare nell'activity precedente (controllare anche il manifest)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                btnCaricaImmagine = findViewById(R.id.btnScegliImmagineEvento);
                btnAggiungiEvento = findViewById(R.id.btnAggiungiEvento);

                titolo = findViewById(R.id.etTitolo);
                citta = findViewById(R.id.etCitta);
                indirizzo = findViewById(R.id.etIndirizzo);
                email = findViewById(R.id.etEmail);
                link = findViewById(R.id.etLinkNFC);
                data = findViewById(R.id.etData);
                prezzo = findViewById(R.id.etPrezzo);
                telefono = findViewById(R.id.etTelefono);
                inputProjectImage = findViewById(R.id.imageViewAddEvent);
                descrizione= findViewById(R.id.etDescrizioneEvento);

                //creato riferimento allo storage contenente le immagini dell'evento del Gargano
                eventImagesRef = FirebaseStorage.getInstance().getReference().child("event images");
                //creato riferimento al Realtime Database che contiene le informazioni degli eventi
                eventRef = FirebaseDatabase.getInstance().getReference().child("events");


                btnCaricaImmagine.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openGallery();
                    }
                });

                btnAggiungiEvento.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        validateEventData();
                        startActivity(new Intent(getApplicationContext(), MieiEventiActivity.class));
                        finish();
                    }
                });
            }
            else
            {
                //se non è loggato non può aggiungere gli eventi
                startActivity(new Intent(getApplicationContext(), AccediRegistrati.class));
                finish();
            }
    }

    //richiesta permesso per accedere allo storage esterno
    private void openGallery(){
        //Il PackageManager controlla che il permesso sia stato concesso dall'utente in precedenza, altrimenti lo richiede
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.permission_needed))
                        .setMessage(getString(R.string.avviso_permesso))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(AggiungiEventoActivity.this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE},GALLERY_PICK);
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create().show();
            }else {
                ActivityCompat.requestPermissions(this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PICK );
            }


        }else{
            try{
                Intent galleryIntent=new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                if(galleryIntent.resolveActivity(getPackageManager())!= null) {
                    startActivityForResult(galleryIntent, GALLERY_PICK);
                }
            }
            catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
    }

    //il metodo gestisce la risposta dell'utente al metodo requestPermission
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        if(requestCode==GALLERY_PICK){
            if(grantResults.length > 0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, R.string.permission_granted,Toast.LENGTH_SHORT).show();


            }else {
                Toast.makeText(this, R.string.permission_danied,Toast.LENGTH_SHORT).show();
            }
            return;
        }
    }

    //qualora il permesso fosse stato concesso, vengono salvate le informazioni relative all'immagine selezionata dall'utente
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if((requestCode == GALLERY_PICK) && (resultCode  == RESULT_OK) && data != null) {
            imageUri=data.getData();
            inputProjectImage.setImageURI(imageUri);
        }
    }

    //metodo che verifica la validità dei dati inseriti
    private void validateEventData(){

        sTitolo = titolo.getText().toString();
        sCitta = citta.getText().toString();
        sData = data.getText().toString();
        sPrezzo = prezzo.getText().toString();
        sTelefono = telefono.getText().toString();
        sIndirizzo = indirizzo.getText().toString();
        sEmail = email.getText().toString();
        sLink = link.getText().toString();
        sDescrione= descrizione.getText().toString();

        if(imageUri == null) {
            Toast.makeText(this,R.string.mancanzaFoto,Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(sTitolo)) {
            Toast.makeText(this,R.string.mancanzaTitolo,Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(sCitta)) {
            Toast.makeText(this,R.string.mancanzaCitta,Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(sData)) {
            Toast.makeText(this,R.string.mancanzaData,Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(sPrezzo)) {
            Toast.makeText(this,R.string.mancanzaPrezzo,Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(sTelefono)) {
            Toast.makeText(this,R.string.mancanzaTelefono,Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(sIndirizzo)) {
            Toast.makeText(this,R.string.mancanzaIndirizzo,Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(sEmail)) {
            Toast.makeText(this,R.string.mancanzaEmail,Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(sDescrione))
        {
            Toast.makeText(this,R.string.mancanzaDescrizione,Toast.LENGTH_SHORT).show();
        }
        else {
            storeEventInformation();
        }
    }

    //salvataggio immagine nello Storage
    private void storeEventInformation() {
        Calendar calendar=Calendar.getInstance();

        SimpleDateFormat currentDate=new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate=currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime=new SimpleDateFormat("HH:mm:ss a");
        saveCurrentTime=currentTime.format(calendar.getTime());

        //chiave dell'immagine nello Storage
        randomKey=saveCurrentDate + saveCurrentTime;

        final StorageReference filePath=eventImagesRef.child(imageUri.getLastPathSegment() + randomKey + ".jpg");

        final UploadTask uploadTask=filePath.putFile(imageUri);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                String message= e.toString();
                Toast.makeText(AggiungiEventoActivity.this,R.string.errore+ message,Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Task<Uri> urlTask=uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if(!task.isSuccessful())
                        {
                            throw task.getException();
                        }

                        downloadImageUrl=filePath.getDownloadUrl().toString();
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if(task.isSuccessful())
                        {
                            downloadImageUrl=task.getResult().toString();
                            //salvataggio informazioni relative al nuovo evento
                            saveEventInfoIntoDatabase();
                        }
                    }
                });
            }
        });
    }

    //salvataggio informazioni relative al nuovo evento
    private void saveEventInfoIntoDatabase(){
        HashMap<String,Object> cityMap = new HashMap<>();
        cityMap.put("cid", randomKey);
        cityMap.put("date", saveCurrentDate);
        cityMap.put("time",saveCurrentTime);
        cityMap.put("titolo",sTitolo);
        cityMap.put("citta",sCitta);
        cityMap.put("indirizzo",sIndirizzo);
        cityMap.put("email",sEmail);
        cityMap.put("link",sLink);
        cityMap.put("prezzo",sPrezzo);
        cityMap.put("data",sData);
        cityMap.put("telefono",sTelefono);
        cityMap.put("image",downloadImageUrl);
        cityMap.put("userID",userID);
        cityMap.put("descrizione",sDescrione);

        eventRef.child(randomKey).updateChildren(cityMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    Toast.makeText(AggiungiEventoActivity.this,R.string.successoAggiutaEvento,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    String message=task.getException().toString();
                    Toast.makeText(AggiungiEventoActivity.this,R.string.errore + message,Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


}
