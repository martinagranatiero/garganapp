package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class DettagliCittaActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView descrizione;
    TextView titolo;
    ImageView image;
    String title;
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    Button btnMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dettagli_citta);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.dettaglio_citta);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String k = getIntent().getStringExtra("key");
        descrizione = findViewById(R.id.tvDescrizione);
        titolo = findViewById(R.id.tvNomeCitta);
        image = findViewById(R.id.imageUpload);
        btnMaps = findViewById(R.id.btnIndicazioniDettaglioEvento);

        //creato riferimento allo storage di Firebase che contiene l'immagine della città cliccata
        mStorageRef = FirebaseStorage.getInstance().getReference("city images").child(k);
        //creato riferimento al Realtime Database che contiene le informazioni della città cliccata
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("cities").child(k);


        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //prendo i dati dal database e li visualizzo
                title = dataSnapshot.child("titolo").getValue().toString();
                String descr = dataSnapshot.child("description").getValue().toString();
                String imgUrl = dataSnapshot.child("image").getValue().toString();
                titolo.setText(title);

                //Per mostrare i paragrafi nella descrizione
                descr = descr.replace("\\n","\n");
                descrizione.setText(descr);

                //per visualizzare l'immagine
                Picasso.get().load(imgUrl).into(image);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //al click del bottone "Ottieni indicazioni" si aprirà Maps
        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String indirizzo = title;
                indirizzo = indirizzo.replace(' ', '+');
                Intent mappaIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + indirizzo));
                startActivity(mappaIntent);

            }
        });
    }
}
