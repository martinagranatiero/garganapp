package it.uniba.minimal.garganapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ChiSiamoActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button btnContattaci;
    String[]addresseToSendEmail = {
            "garganappminimal@gmail.com"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_siamo);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.toolbar_name_chisiamo);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Pulsante contattaci
        btnContattaci = findViewById(R.id.btnContattaci);
        btnContattaci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                composeEmail(addresseToSendEmail,getString(R.string.informazioniContattaci));
            }
        });
    }

    //Intent per passare all'email
    public void composeEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
