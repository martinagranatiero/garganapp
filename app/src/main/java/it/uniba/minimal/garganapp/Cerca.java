package it.uniba.minimal.garganapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Cerca extends Fragment {

    RecyclerView recyclerViewCitta, recyclerViewEventi;
    ImageAdapter adapterCitta;
    List<Citta> cittaList;
    List<Eventi> eventiList;
    EditText cittaCercata;
    Button cercaBtn;
    TextView eventiCitta, cittaVuota, eventiVuota;
    EventiAdapter adapterEventi;

    public Cerca() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cerca, container, false);

        cittaCercata = v.findViewById(R.id.search_field);
        cercaBtn = v.findViewById(R.id.searchButton);
        recyclerViewCitta = v.findViewById(R.id.result_list);
        recyclerViewCitta.setHasFixedSize(true);
        recyclerViewCitta.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerViewEventi = v.findViewById(R.id.event_list);
        recyclerViewEventi.setHasFixedSize(true);
        recyclerViewEventi.setLayoutManager(new LinearLayoutManager(getActivity()));

        eventiCitta = v.findViewById(R.id.eventiCitta);
        eventiCitta.setVisibility(View.GONE);
        cittaVuota = v.findViewById(R.id.cittaVuota);
        cittaVuota.setVisibility(View.GONE);
        eventiVuota = v.findViewById(R.id.eventiVuota);
        eventiVuota.setVisibility(View.GONE);

        cittaList = new ArrayList<>();
        eventiList = new ArrayList<>();


        cercaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //vengono resettate le liste contenenti le città e gli eventi
                cittaList.clear();
                eventiList.clear();

                //viene resa invisibile la textView che spiega che non è stata ancora inserita la città nell'app
                cittaVuota.setVisibility(View.GONE);
                //viene resa invisibile la textView "eventi in programma in questa città"
                eventiCitta.setVisibility(View.GONE);
                //viene resa invisibile la textView che spiega che non ci sono eventi in programma nella città cercata
                eventiVuota.setVisibility(View.GONE);

                //ricerca della città inserita dall'utente
                final String varCitta = cittaCercata.getText().toString();
                //creato riferimento al Realtime Database che contiene le informazioni delle città
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("cities");
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        //ciclo per prendere tutti le città presenti nel database
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            Citta citta = ds.getValue(Citta.class);

                            //controllo se il nome della città presa dal database contiene
                            //la sequenza di caratteri inserita dall'utente
                            if (citta.getTitolo().toLowerCase().contains(varCitta.toLowerCase())) {
                                cittaList.add(citta);
                            }

                            //setto l'adapter per la visualizzazione delle card città
                            adapterCitta = new ImageAdapter(getActivity(), cittaList);
                            adapterCitta.notifyDataSetChanged();
                            //cliccando sulla card relativa alla città posso visualizzare i dettagli di tale città
                            adapterCitta.setOnItemClickListener(new ImageAdapter.onItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    Citta selectedItem = cittaList.get(position);
                                    String selectedKey = selectedItem.getCid();
                                    Intent intent= new Intent(getActivity(), DettagliCittaActivity.class);
                                    intent.putExtra("key",selectedKey);
                                    startActivity(intent);
                                }

                            });
                            recyclerViewCitta.setAdapter(adapterCitta);
                        }

                        //messaggio di assenza città cercata
                        if (cittaList.isEmpty()) {
                            cittaVuota.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });

                //ricerca degli eventi in programma nella città inserita dall'utente
                //creato riferimento al Realtime Database che contiene le informazioni degli eventi
                DatabaseReference refEventi = FirebaseDatabase.getInstance().getReference("events");
                refEventi.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        //ciclo per prendere tutti gli eventi presenti nel database
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            Eventi eventi = ds.getValue(Eventi.class);

                            //controllo se la città in cui si svolgerà l'evento contiene
                            //la sequenza di caratteri inserita dall'utente
                            if (eventi.getCitta().toLowerCase().contains(varCitta.toLowerCase())) {
                                eventiList.add(eventi);
                            }

                            //setto l'adapter per la visualizzazione degli eventi
                            adapterEventi = new EventiAdapter(getActivity(), eventiList);
                            adapterEventi.notifyDataSetChanged();
                            //cliccando sulla card dell'evento posso visualizzare i dettagli di tale evento
                            adapterEventi.setOnItemClickListener(new EventiAdapter.onItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    Eventi selectedItem = eventiList.get(position);
                                    String selectedKey = selectedItem.getCid();
                                    Intent intent = new Intent(getActivity(), DettagliEventoActivity.class);
                                    intent.putExtra("key",selectedKey);
                                    startActivity(intent);
                                }

                            });
                            recyclerViewEventi.setAdapter(adapterEventi);

                            //se non ci sono eventi rendo visibile la textView che spiega che non ci sono eventi in programma
                            if (eventiList.isEmpty()) {
                                eventiVuota.setVisibility(View.VISIBLE);
                                eventiCitta.setVisibility(View.GONE);
                            } else {
                                //altrimenti rendo visibile la textView "Eventi in programma in questa città"
                                eventiCitta.setVisibility(View.VISIBLE);
                                eventiVuota.setVisibility(View.GONE);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }
        });
        return v;
    }
}
