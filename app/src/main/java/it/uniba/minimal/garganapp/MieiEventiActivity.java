package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MieiEventiActivity extends AppCompatActivity {

    FloatingActionButton fab;
    private FirebaseAuth fAuth;
    private EventiAdapter mAdapter;
    private List<Eventi> mEventi;
    private RecyclerView mRecyclerView;
    private DatabaseReference mDatabaseRef;
    private TextView assenzaEventi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miei_eventi);

        fAuth = FirebaseAuth.getInstance();
        assenzaEventi=findViewById(R.id.assenzaEventi);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.mieiEventi);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = findViewById(R.id.recyclerViewEventi);
        //la modifica del contenuto dell'adattatore non modifica l'altezza o la larghezza
        mRecyclerView.setHasFixedSize(true);
        //controlla le disposizioni di layout personalizzate
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mEventi = new ArrayList<>();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("events");

        //se l'utente è loggato avrà la possibilità di vedere gli eventi inseriti e di aggiungerne di nuovi
        if (fAuth.getCurrentUser() != null) {
            final String userId = fAuth.getUid();

            mDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    mEventi.clear();
                    for(DataSnapshot postShapshot : dataSnapshot.getChildren()) {
                        //vengono presi dal db tutti gli eventi disponibili
                        Eventi eventi = postShapshot.getValue(Eventi.class);
                        String userEvent=eventi.userID;

                        //vengono selezionati solo gli eventi inseriti dall'utente loggato
                        if (userId.equals(userEvent)) {
                            mEventi.add(eventi);
                        }
                    }

                    //qualora non avesse inserito nessun evento compare una text view che lo informa di non aver inserito nessun evento
                    // e che eventualmente può inserirli cliccando sul fab
                    if(mEventi.isEmpty()) {
                        assenzaEventi.setVisibility(View.VISIBLE);
                    }
                    else {
                        assenzaEventi.setVisibility(View.GONE);
                    }

                    mAdapter = new EventiAdapter(MieiEventiActivity.this, mEventi);
                    mAdapter.setOnItemClickListener(new EventiAdapter.onItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            //qualora si clicchi sull'evento, l'utente potrà vedere nello specifico l'evento inserito con la possibilità di modificarlo o eliminarlo
                            Eventi selectedItem = mEventi.get(position);
                            String selectedKey = selectedItem.getCid();
                            Intent intent = new Intent(MieiEventiActivity.this, VisualizzazioneEventoActivity.class);
                            intent.putExtra("key",selectedKey);
                            startActivity(intent);
                        }

                    });

                    mRecyclerView.setAdapter(mAdapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(MieiEventiActivity.this, databaseError.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
            fab = findViewById(R.id.fabEvents);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startAggiungiEventoActivity();
                }
            });

        } else {
            //se l'utente non è loggato non potrà vedere i suoi eventi e verrà riportato all'activity accedi/registrati
            startActivity(new Intent(getApplicationContext(),AccediRegistrati.class));
            Toast.makeText(MieiEventiActivity.this, R.string.noLoginMieiEventi, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void startAggiungiEventoActivity() {
        //porta all'activity AggiungiEvento che permette all'utente di aggiungere un suo evento
        startActivity(new Intent(MieiEventiActivity.this, AggiungiEventoActivity.class));
    }
}
