package it.uniba.minimal.garganapp;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;


public class AggiungiCittaActivity extends AppCompatActivity {

    Toolbar toolbar;
    private Button btnCaricaImmagine;
    private Button btnAggiungiCitta;
    private ImageView inputProjectImage;
    private EditText titolo;
    private EditText descrizione;
    private static final int GALLERY_PICK = 1;
    private Uri imageUri;
    private String title,descr,saveCurrentDate,saveCurrentTime,randomKey,downloadImageUrl;
    private StorageReference cityImagesRef;
    private DatabaseReference cityRef;
    private static final String TAG = "MINIMAL_PERMISSIONS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aggiungi_citta);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.toolbar_name_aggiungicitta);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnCaricaImmagine=findViewById(R.id.btnUpload);
        btnAggiungiCitta=findViewById(R.id.btnAggiungiCitta);
        titolo=findViewById(R.id.txtInputCitta);
        descrizione=findViewById(R.id.etDescrizioneCitta);
        inputProjectImage=findViewById(R.id.imageUpload);

        //creato riferimento allo storage contenente le immagini di città del Gargano
        cityImagesRef= FirebaseStorage.getInstance().getReference().child("city images");
        //creato riferimento al Realtime Database che contiene le informazioni delle città
        cityRef=FirebaseDatabase.getInstance().getReference().child("cities");

        btnCaricaImmagine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        btnAggiungiCitta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              validateCityData();
              startActivity(new Intent(getApplicationContext(),ElencoCittaActivity.class));
              finish();
            }
        });

    }
//richiesta permesso per accedere allo storage esterno
    private void openGallery(){
        //Il PackageManager controlla che il permesso sia stato concesso dall'utente in precedenza, altrimenti lo richiede
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED){
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
                    new AlertDialog.Builder(this)
                            .setTitle(getString(R.string.permission_needed))
                            .setMessage(getString(R.string.avviso_permesso))
                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(AggiungiCittaActivity.this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE},GALLERY_PICK);
                                }
                            })
                            .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create().show();
                }else {
                    ActivityCompat.requestPermissions(this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PICK );
                }


            }else{
                try{
                    //intent alla galleria del dispostivito dell'utente
                    Intent galleryIntent=new Intent();
                    galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                    galleryIntent.setType("image/*");
                    if(galleryIntent.resolveActivity(getPackageManager())!= null) {
                        startActivityForResult(galleryIntent, GALLERY_PICK);
                    }
                    }
                catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }

        }


    @Override
    //il metodo gestisce la risposta dell'utente al metodo requestPermission
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        if(requestCode==GALLERY_PICK){
            if(grantResults.length > 0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, R.string.permission_granted,Toast.LENGTH_SHORT).show();


            }else {
                Toast.makeText(this, R.string.permission_danied,Toast.LENGTH_SHORT).show();
            }
            return;
        }
    }

    //qualora il permesso fosse stato concesso, vengono salvate le informazioni relative all'immagine selezionata dall'utente
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if((requestCode == GALLERY_PICK) && (resultCode  == RESULT_OK) && data != null)
        {
          imageUri=data.getData();
          inputProjectImage.setImageURI(imageUri);
        }
    }

    //metodo che verifica la validità dei dati inseriti
      private void validateCityData(){
      title=titolo.getText().toString();
      descr=descrizione.getText().toString();
      if(imageUri == null)
      {
          Toast.makeText(this,R.string.mancanzaFoto,Toast.LENGTH_SHORT).show();
      }
      else if(TextUtils.isEmpty(descr))
      {
          Toast.makeText(this,R.string.mancanzaDescrizione,Toast.LENGTH_SHORT).show();
      } else if (TextUtils.isEmpty(title))
      {
          Toast.makeText(this,R.string.mancanzaTitolo,Toast.LENGTH_SHORT).show();
      } else {
          storeCityInformation();
      }
    }

    //salvataggio immagine nello Storage
    private void storeCityInformation()
    {
        Calendar calendar=Calendar.getInstance();

        SimpleDateFormat currentDate=new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate=currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime=new SimpleDateFormat("HH:mm:ss a");
        saveCurrentTime=currentTime.format(calendar.getTime());

        //chiave dell'immagine nello Storage
        randomKey=saveCurrentDate + saveCurrentTime;

        final StorageReference filePath=cityImagesRef.child(imageUri.getLastPathSegment() + randomKey + ".jpg");

        final UploadTask uploadTask=filePath.putFile(imageUri);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                String message= e.toString();
                Toast.makeText(AggiungiCittaActivity.this,R.string.errore + message,Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Task<Uri> urlTask=uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if(!task.isSuccessful())
                        {
                            throw task.getException();
                        }

                         downloadImageUrl=filePath.getDownloadUrl().toString();
                         return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if(task.isSuccessful())
                        {
                            downloadImageUrl=task.getResult().toString();

                            //salvataggio informazioni relative alla nuova città
                            saveCityInfoIntoDatabase();
                        }
                    }
                });
            }
        });
    }

    //salvataggio informazioni relative alla nuova città
    private void saveCityInfoIntoDatabase(){
        HashMap<String,Object> cityMap = new HashMap<>();
        cityMap.put("cid", randomKey);
        cityMap.put("date", saveCurrentDate);
        cityMap.put("time",saveCurrentTime);
        cityMap.put("description", descr);
        cityMap.put("titolo",title);
        cityMap.put("image",downloadImageUrl);
        cityRef.child(randomKey).updateChildren(cityMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
             if(task.isSuccessful())
             {
                 Toast.makeText(AggiungiCittaActivity.this,R.string.successoAggiutaCitta,Toast.LENGTH_SHORT).show();
             }
             else
             {
                 String message=task.getException().toString();
                 Toast.makeText(AggiungiCittaActivity.this,R.string.errore + message,Toast.LENGTH_SHORT).show();
             }
            }
        });


    }
}
