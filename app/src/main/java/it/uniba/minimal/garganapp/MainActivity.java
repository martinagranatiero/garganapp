package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener, NavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView  bottomNavigationView;
    Toolbar toolbar;
    NavigationView navigationView;
    FirebaseAuth fAuth;

    private DrawerLayout mNavDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* All'interno del layout nav_drawer_layout è incluso il layout relativo alla main Activity.
           Questo, fa riferimento al navigation drawer */
        setContentView(R.layout.nav_drawer_layout);

        //Per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.menu_toolbar_title);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Utile per visualizzare l'hamburger + animazione
        mNavDrawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mNavDrawer,toolbar,R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mNavDrawer.addDrawerListener(toggle);
        toggle.syncState();

        //verifica se c'è un utente loggato
        fAuth=FirebaseAuth.getInstance();

        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        /*Metodo che verifica se l'utente è loggato, è in caso affermativo rimuove dalla navigation drawer
         accedi/registrati */
        hideItem(navigationView);


        //Per settare la bottomNavigation
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(R.id.navigation_home);

    }


    /*Nasconde item accedi/registrati dalla navigation drawer se l'utente è loggato ed
    aggiunge l'icona del logout e viceversa */

    private void hideItem(NavigationView navigationView) {
        Menu nav_Menu = navigationView.getMenu();
        if(fAuth.getCurrentUser() != null) {
            nav_Menu.findItem(R.id.nav_login).setVisible(false);
            nav_Menu.findItem(R.id.nav_logout).setVisible(true);
        }else{
            nav_Menu.findItem(R.id.nav_login).setVisible(true);
            nav_Menu.findItem(R.id.nav_logout).setVisible(false);
        }

    }

    //Per non uscire dall'activity dopo la pressione del tasto back (legato al navigation Drawer)
    @Override
    public void onBackPressed(){
        if(mNavDrawer.isDrawerOpen(GravityCompat.START)){
            mNavDrawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    //Inserimento opzioni del menu all'interno della toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_toolbar,menu);
        return true;
    }

    //Utile per la selezione dell'item di login nella toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id==R.id.action_login){
            goLoginActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    //Metodo contentenente un Intent esplicito che rimanda alla sezione login (dall'item login della toolbar)
    private void goLoginActivity() {
        final Intent intent = new Intent(this,AccediRegistrati.class);
        startActivity(intent);
    }


    Esplora esploraFragment = new Esplora();
    Cerca cercaFragment = new Cerca();
    EventiHomeFragment home = new EventiHomeFragment();

    /* Utile per permettere il passaggio, tramite gli item del bottom menu, da un fragment all'altro.
        Presente anche all'interno, le sezioni del Navigation Drawer,
        contenenti gli intent espliciti per passare ad altre activity. */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch(menuItem.getItemId()){
            case R.id.navigation_add_event:
                Intent intentMieiEventi = new Intent(MainActivity.this,MieiEventiActivity.class);
                startActivity(intentMieiEventi);
                return true;
            case R.id.navigation_explore:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in,R.anim.fade_out).replace(R.id.container,esploraFragment).commit();
                return true;
            case R.id.navigation_home:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in,R.anim.fade_out).replace(R.id.container,home).commit();
                return true;
            case R.id.navigation_search:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in,R.anim.fade_out).replace(R.id.container,cercaFragment).commit();
                return true;
            case R.id.nav_login:
                Intent intent = new Intent(MainActivity.this,AccediRegistrati.class);
                startActivity(intent);
                return true;
            case R.id.nav_chisiamo:
                startActivity(new Intent(MainActivity.this, ChiSiamoActivity.class));
                return true;
                case R.id.nav_area_staff:
                    Intent intentAreaStaff = new Intent(MainActivity.this,AccediAreaStaffActivity.class);
                startActivity(intentAreaStaff);
                return true;
            case R.id.nav_bussola:
                startActivity(new Intent(MainActivity.this, Bussola.class));
                return true;
            case R.id.nav_profilo:
                startActivity(new Intent(MainActivity.this, Profilo.class));
                return true;
            case R.id.nav_logout:
                logout();
        }
        return false;
    }

    //Metodo che permette all'utente di effettuare il logout dalla navigation drawer
    private void logout() {
        fAuth.getInstance().signOut();
        Toast.makeText(MainActivity.this,R.string.logout,Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        finish();
    }

}
