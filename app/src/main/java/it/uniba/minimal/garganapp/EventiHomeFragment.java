package it.uniba.minimal.garganapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class EventiHomeFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private DatabaseReference mDatabase;
    private EventiAdapter mAdapter;
    private List<Eventi> mEventi;

    public EventiHomeFragment() {
        //costruttore pubblico vuoto
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate del layout per questo fragment
        View v  = inflater.inflate(R.layout.fragment_eventi_home, container, false);

        mRecyclerView = v.findViewById(R.id.recyclerViewEventi);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mEventi = new ArrayList<>();

        //Per ottenere il riferimento agli eventi
        mDatabase = FirebaseDatabase.getInstance().getReference().child("events");
        mDatabase.keepSynced(true);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //ciclo per prendere tutte gli eventi presenti nel database
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Eventi eventi = postSnapshot.getValue(Eventi.class);
                    mEventi.add(eventi);
                }

                //setto l'adapter per la visualizzazione delle card eventi
                mAdapter = new EventiAdapter(getActivity(), mEventi);
                mAdapter.setOnItemClickListener(new EventiAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        Eventi selectedItem = mEventi.get(position);
                        String selectedKey = selectedItem.getCid();
                        Intent intent = new Intent(getActivity(), DettagliEventoActivity.class);
                        intent.putExtra("key",selectedKey);
                        startActivity(intent);
                    }

                });

                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

        return v;

    }
}