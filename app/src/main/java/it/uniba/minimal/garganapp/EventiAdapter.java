package it.uniba.minimal.garganapp;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class EventiAdapter extends RecyclerView.Adapter<EventiAdapter.ImageViewHolder> {

    private Context mContext;
    private List<Eventi> mEventi;
    private onItemClickListener mListener;

    public EventiAdapter(Context mContext, List<Eventi> mEventi) {
        this.mContext = mContext;
        this.mEventi = mEventi;
    }

    //per impostare il layout degli item della RecyclerView
    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.event_row, parent, false);
        return new ImageViewHolder(v);
    }

    //per aggiornare i contenuti di un elemento  della RecyclerView in una determinata posizione
    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        Eventi eventiCurrent = mEventi.get(position);
        holder.textViewTitolo.setText(eventiCurrent.getTitolo());
        holder.textViewCitta.setText(eventiCurrent.getCitta());
        holder.textViewData.setText(eventiCurrent.getData());
        Picasso.get().load(eventiCurrent.getImage()).fit().centerCrop().into(holder.imageView);
    }

    //Restituisce il numero totale di item nella sorgente dati conservati dall'adapter.
    @Override
    public int getItemCount() {
        return mEventi.size();
    }

    public class ImageViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textViewTitolo, textViewCitta, textViewData;
        public ImageView imageView;

        //per salvare i riferimenti agli elementi della UI
        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitolo = itemView.findViewById(R.id.eventTitle);
            textViewCitta = itemView.findViewById(R.id.eventDescription);
            textViewData = itemView.findViewById(R.id.eventDate);
            imageView = itemView.findViewById(R.id.eventImage);

            itemView.setOnClickListener(this);
        }

        //per ottenere la posizione dell'evento cliccato ed eseguire azioni aggiuntive
        @Override
        public void onClick(View v) {
            if(mListener != null) {
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION) {
                    mListener.onItemClick(position);
                }
            }
        }


    }

    public interface onItemClickListener {
        void onItemClick(int position);

    }
    public void setOnItemClickListener(onItemClickListener listener) {
        mListener = listener;
    }
}
