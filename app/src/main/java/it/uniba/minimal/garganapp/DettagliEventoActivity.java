package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class DettagliEventoActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback, NfcAdapter.OnNdefPushCompleteCallback {

    Toolbar toolbar;
    TextView titolo, citta, indirizzo, prezzo, data, descrizione, nfc;
    ImageView image;

    Button btnMaps, btnGmail, btnRubrica, btnNFC;
    String emailOrganizzatore;
    String telefonoOrganizzatore;
    String location;
    String link;

    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dettagli_evento);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.dettagli_evento);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String k = getIntent().getStringExtra("key");
        titolo = findViewById(R.id.titolotv);
        citta = findViewById(R.id.cittatv);
        indirizzo = findViewById(R.id.indirizzotv);
        prezzo = findViewById(R.id.prezzotv);
        data = findViewById(R.id.datatv);
        image = findViewById(R.id.imageUpload);
        descrizione = findViewById(R.id.tvDescrizioneEvento);
        nfc = findViewById(R.id.tvnfc);

        nfc.setVisibility(View.GONE);

        //creato riferimento allo storage che contiene l'immagine dell'evento cliccato
        mStorageRef = FirebaseStorage.getInstance().getReference("event images").child(k);
        //creato riferimento al Realtime Database che contiene le informazioni dell'evento cliccato
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("events").child(k);

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //prendo i dati dal database
                String title = dataSnapshot.child("titolo").getValue().toString();
                String city = dataSnapshot.child("citta").getValue().toString();
                location = dataSnapshot.child("indirizzo").getValue().toString();
                String price = dataSnapshot.child("prezzo").getValue().toString();
                String date = dataSnapshot.child("data").getValue().toString();
                String imgUrl = dataSnapshot.child("image").getValue().toString();
                String descr = dataSnapshot.child("descrizione").getValue().toString();

                emailOrganizzatore = dataSnapshot.child("email").getValue().toString();
                telefonoOrganizzatore = dataSnapshot.child("telefono").getValue().toString();

                link = dataSnapshot.child("link").getValue().toString();

                //se c'è il link mostra il messaggio della funzionalità disponibile
                if (!link.isEmpty()) {
                    nfc.setVisibility(View.VISIBLE);
                }

                //per visualizzare i dati presi dal database
                titolo.setText(title);
                citta.setText(city);
                indirizzo.setText(location);
                prezzo.setText(price);
                data.setText(date);
                descr = descr.replace("\\n","\n");
                descrizione.setText(descr);
                Picasso.get().load(imgUrl).into(image);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //funzionamento dell'NFC (si chiamano i due metodi per inviare e ricevere)
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if(nfcAdapter != null) {
            nfcAdapter.setNdefPushMessageCallback(this, this);
            nfcAdapter.setOnNdefPushCompleteCallback(this, this);
        }

        btnGmail = findViewById(R.id.btnEmailDettaglioEvento);
        btnMaps = findViewById(R.id.btnIndicazioniDettaglioEvento);
        btnRubrica = findViewById(R.id.btnTelefonoDettaglioEvento);

        //al click del bottone "Invia un'email all'organizzatore" si apre l'app per l'invio di email
       btnGmail.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               composeEmail(emailOrganizzatore,getString(R.string.informazioniContattaci));
           }
       });

       //al click del bottone "Ottieni indicazioni stradali" si apre Maps
       btnMaps.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String indirizzo = location;
               indirizzo = indirizzo.replace(' ', '+');
               Intent mappaIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + indirizzo));
               startActivity(mappaIntent);

           }
       });

       //al click del bottone "Chiama l'organizzatore" si apre il dialer
      btnRubrica.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              dialPhoneNumber(telefonoOrganizzatore);
          }
      });

    }

    //Intent che porta al Dialer per chiamare l'organizzatore
    public void dialPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    //Intent per inviare email all'organizzatore
    private void composeEmail(String addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this

        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { addresses });
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    //metodo che permette di inviare dati tramite l'NFC
    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        NdefMessage msg = new NdefMessage(new NdefRecord[] {
                NdefRecord.createUri(link)
        });
        return msg;
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {}

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {}
}
