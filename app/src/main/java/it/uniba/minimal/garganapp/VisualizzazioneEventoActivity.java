package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class VisualizzazioneEventoActivity extends AppCompatActivity {

    EditText titolo, citta, descrizione, data, prezzo, telefono, indirizzo, email, link;
    Button modificaEvento, caricaImaagine, eliminaEvento;
    ImageView inputProjectImage;

    String imagePath, downloadImageUrl, title, city, descr, dataEvento, price, tel, address, emailOrg, linkNFC, newPath = "";

    boolean permessoConcesso = false;
    static final int PICK_IMAGE = 1;
    Uri selectedImage;

    DatabaseReference mDatabaseRef;
    StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizzazione_evento);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.dettagli_evento);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String k = getIntent().getStringExtra("key");

        titolo = findViewById(R.id.etTitolo);
        citta = findViewById(R.id.etCitta);
        descrizione = findViewById(R.id.etDescrizioneEvento);
        data = findViewById(R.id.etData);
        prezzo = findViewById(R.id.etPrezzo);
        telefono = findViewById(R.id.etTelefono);
        indirizzo = findViewById(R.id.etIndirizzo);
        email = findViewById(R.id.etEmail);
        link = findViewById(R.id.etLinkNFC);
        inputProjectImage = findViewById(R.id.imageViewAddEvent);

        modificaEvento = findViewById(R.id.btnModificaEvento);
        caricaImaagine = findViewById(R.id.btnScegliImmagineEvento);
        eliminaEvento = findViewById(R.id.btnEliminaEvento);

        //creato riferimento allo storage di Firebase che contiene le immagini degli eventi
        mStorageRef = FirebaseStorage.getInstance().getReference("event images");
        //creato riferimento al Realtime Database che contiene le informazioni dell'evento cliccato
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("events").child(k);

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //se il database contiene questo evento allora prendo i dati dal database e li visualizzo
                if (dataSnapshot.exists()) {
                    title = dataSnapshot.child("titolo").getValue().toString();
                    city = dataSnapshot.child("citta").getValue().toString();
                    descr = dataSnapshot.child("descrizione").getValue().toString();
                    dataEvento = dataSnapshot.child("data").getValue().toString();
                    imagePath = dataSnapshot.child("image").getValue().toString();
                    price = dataSnapshot.child("prezzo").getValue().toString();
                    tel = dataSnapshot.child("telefono").getValue().toString();
                    address = dataSnapshot.child("indirizzo").getValue().toString();
                    emailOrg = dataSnapshot.child("email").getValue().toString();
                    linkNFC = dataSnapshot.child("link").getValue().toString();

                    titolo.setText(title);
                    citta.setText(city);
                    descrizione.setText(descr);
                    data.setText(dataEvento);
                    prezzo.setText(price);
                    telefono.setText(tel);
                    indirizzo.setText(address);
                    email.setText(emailOrg);
                    link.setText(linkNFC);
                    Picasso.get().load(imagePath).into(inputProjectImage);
                } else {
                    //se invece il database non contiene questo evento allora esci da questa activity
                    VisualizzazioneEventoActivity.this.finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //al click del bottone "carica immagine" si richiedono i permessi all'utente
        caricaImaagine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(VisualizzazioneEventoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!=
                        PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(VisualizzazioneEventoActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            PICK_IMAGE);
                else {
                    permessoConcesso = true;
                }

                if(permessoConcesso) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_IMAGE);
                } else {
                    ActivityCompat.requestPermissions(VisualizzazioneEventoActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            PICK_IMAGE);
                }
            }
        });

        //al click del bottone "elimina evento" si porta l'utente alla sezione "Miei Eventi", si rimuove l'evento dal database
        // e si avvisa l'utente dell'avvenuta eliminazione
        eliminaEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VisualizzazioneEventoActivity.this, MieiEventiActivity.class));
                mDatabaseRef.removeValue();
                Toast.makeText(VisualizzazioneEventoActivity.this, R.string.eliminazioneEventoSuccesso, Toast.LENGTH_LONG).show();
            }
        });

        //al click del bottone "salva modifiche" si aggiornano i dati nel database
        modificaEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        //controllo se i seguenti campi sono stati modificati o meno
                        //in caso affermativo aggiorna i dati nel database
                        if (!title.equals(titolo)) {
                            dataSnapshot.getRef().child("titolo").setValue(titolo.getText().toString());
                        }

                        if (!city.equals(citta)) {
                            dataSnapshot.getRef().child("citta").setValue(citta.getText().toString());
                        }

                        if (!descr.equals(descrizione)) {
                            dataSnapshot.getRef().child("descrizione").setValue(descrizione.getText().toString());
                        }

                        if (!dataEvento.equals(data)) {
                            dataSnapshot.getRef().child("data").setValue(data.getText().toString());
                        }

                        if (!price.equals(prezzo)) {
                            dataSnapshot.getRef().child("prezzo").setValue(prezzo.getText().toString());
                        }

                        if (!tel.equals(telefono)) {
                            dataSnapshot.getRef().child("telefono").setValue(telefono.getText().toString());
                        }

                        if (!emailOrg.equals(email)) {
                            dataSnapshot.getRef().child("email").setValue(email.getText().toString());
                        }

                        if (!linkNFC.equals(link)) {
                            dataSnapshot.getRef().child("link").setValue(link.getText().toString());
                        }

                        if (selectedImage != null) {
                            newPath = selectedImage.getLastPathSegment();

                            //caricamento della nuova immagine nello storage
                            final StorageReference riversRef = mStorageRef.child(selectedImage.getLastPathSegment()+".jpg");

                            final UploadTask uploadTask = riversRef.putFile(selectedImage);

                            //serve per ottenere il link dell'immagine nello storage per inserirlo
                            //nel campo image dell'evento che si sta modificando
                            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                        @Override
                                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                            if(!task.isSuccessful()) {
                                                throw task.getException();
                                            }

                                            downloadImageUrl = riversRef.getDownloadUrl().toString();
                                            return riversRef.getDownloadUrl();
                                        }
                                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            if(task.isSuccessful()) {
                                                downloadImageUrl = task.getResult().toString();
                                                dataSnapshot.getRef().child("image").setValue(downloadImageUrl);
                                            }
                                        }
                                    });
                                }
                            });
                        }

                        //una volta modificato i dati si porta l'utente nella sezione "Miei Eventi"
                        // e si avvisa l'utente dell'avvenuta modifica
                        VisualizzazioneEventoActivity.this.finish();
                        Toast.makeText(VisualizzazioneEventoActivity.this, R.string.modificaEventoSuccesso, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    //permessi per accedere alla galleria
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permessoConcesso = true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && null != data) {
            if (resultCode == Activity.RESULT_OK) {
                selectedImage = data.getData();
                inputProjectImage.setImageURI(selectedImage);
            }
        }
    }
}
