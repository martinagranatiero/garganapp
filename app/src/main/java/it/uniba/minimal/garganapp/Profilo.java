package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class Profilo extends AppCompatActivity {

    TextView nome;
    TextView cognome;
    TextView email;
    TextView cambiaPassword;
    TextView telefono;
    TextView nomeBenvenuto;
    Button logout;

    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userID;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilo);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.toolbar_name_profilo);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nome = findViewById(R.id.nomeET);
        cognome = findViewById(R.id.cognomeET);
        email = findViewById(R.id.emailET);
        telefono = findViewById(R.id.telefonoET);
        nomeBenvenuto = findViewById(R.id.nome);
        cambiaPassword=findViewById(R.id.cambiaPassword);

        //set disabilitato EditText
        nome.setEnabled(false);
        cognome.setEnabled(false);
        email.setEnabled(false);
        telefono.setEnabled(false);

        final ToggleButton modificaInformazioniTB = findViewById(R.id.modificaInformazioni);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        //se l'utente è loggato vengono prese dal database le sue informazioni grazie all'userID
        if (fAuth.getCurrentUser() != null) {
            userID = fAuth.getCurrentUser().getUid();
                final DocumentReference documentReference = fStore.collection("users").document(userID);
                documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        nomeBenvenuto.setText(documentSnapshot.getString("name"));
                        nome.setText(documentSnapshot.getString("name"));
                        cognome.setText(documentSnapshot.getString("surname"));
                        email.setText(documentSnapshot.getString("email"));
                        telefono.setText(documentSnapshot.getString("telefono"));
                    }
                });

                //gestione del ToggleButton per la modifica delle informazioni
                modificaInformazioniTB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            nome.setEnabled(true);
                            cognome.setEnabled(true);
                            telefono.setEnabled(true);

                        } else {
                            nome.setEnabled(false);
                            cognome.setEnabled(false);
                            email.setEnabled(false);
                            telefono.setEnabled(false);

                            //salva le modifiche eventuali apportate nel database
                            final String varNome = nome.getText().toString();
                            final String varCognome = cognome.getText().toString();
                            final String varTelefono = telefono.getText().toString();
                            final String varEmail = email.getText().toString().trim();
                            userID = fAuth.getCurrentUser().getUid();
                            DocumentReference documentReference = fStore.collection("users").document(userID);
                            Map<String, Object> user = new HashMap<>();
                            user.put("name", varNome);
                            user.put("surname", varCognome);
                            user.put("telefono", varTelefono);
                            user.put("email", varEmail);
                            documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d("TAG", R.string.registrazioneSuccesso + userID);
                                }

                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("TAG", R.string.registrazioneFallita + e.toString());
                                }
                            });
                        }
                    }
                });

                //sezione per reimpostare la password
                cambiaPassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final EditText resetMail = new EditText(v.getContext());
                        final AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(v.getContext());
                        passwordResetDialog.setTitle(R.string.passwordDaModificare);
                        passwordResetDialog.setMessage(R.string.emailPasswordCambiata);
                        passwordResetDialog.setView(resetMail);

                        passwordResetDialog.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //estratta email e invio reset link
                                String mail = resetMail.getText().toString();

                                fAuth.sendPasswordResetEmail(mail).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(Profilo.this, R.string.emailPasswordDimenticataInviata, Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(Profilo.this, R.string.emailPasswordDimenticataRigettata + e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });

                        passwordResetDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //chiusura del dialogo
                            }
                        });

                        passwordResetDialog.create().show();
                    }
                });

                //logout dall'applicazione
                logout = findViewById(R.id.logout);
                logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Profilo.this.finish();
                        fAuth.getInstance().signOut();
                        Toast.makeText(Profilo.this, R.string.logout, Toast.LENGTH_SHORT).show();
                    }
                });
            }

        else {
            //qualora l'utente non sia loggato verrà mostrata l'activity relativa all'accesso-registrazione
            startActivity(new Intent(getApplicationContext(),AccediRegistrati.class));
            Toast.makeText(Profilo.this, R.string.noLoginProfilo, Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
