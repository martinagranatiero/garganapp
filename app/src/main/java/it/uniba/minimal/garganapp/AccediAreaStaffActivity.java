package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AccediAreaStaffActivity extends AppCompatActivity {

    Button accedi;
    EditText emailAccedi, passwordAccedi;
    FirebaseAuth firebaseAuth;
    private String keyStaff="1x9ffcgdkrgqfNNQJDUZsS0yzc62";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accedi_area_staff);

        firebaseAuth=FirebaseAuth.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.toolbar_name_areastaff);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        emailAccedi=findViewById(R.id.textemail);
        passwordAccedi=findViewById(R.id.textPassword);
        accedi=findViewById(R.id.accediButton);

        //se le credenziali sono inserite nella sezione Authetification di Firebase e corrispondono a quelle delle dello staff si può accedere
        if (firebaseAuth.getCurrentUser() != null && firebaseAuth.getUid().equals(keyStaff)) {
            startActivity(new Intent(getApplicationContext(),DashboardAreaStaff.class));
            finish();
        }


        accedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String varEmail= emailAccedi.getText().toString().trim();
                String varPassword= passwordAccedi.getText().toString().trim();

                //controllo che email e password siano inseriti
                if(TextUtils.isEmpty(varEmail))
                {
                    emailAccedi.setError(getResources().getString(R.string.emailObbligatoria));
                    return;
                }

                if(TextUtils.isEmpty(varPassword))
                {
                    passwordAccedi.setError(getResources().getString(R.string.passwordObbligatoria));
                    return;
                }
                firebaseAuth.signInWithEmailAndPassword(varEmail,varPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {

                            if(firebaseAuth.getUid().equals(keyStaff))
                            {
                                //se le credenziali sono corrette si accede all'area privata dello Staff
                                Intent intent = new Intent(AccediAreaStaffActivity.this,DashboardAreaStaff.class);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Toast.makeText(AccediAreaStaffActivity.this,getResources().getString(R.string.areaStaffNegato),Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(AccediAreaStaffActivity.this,getResources().getString(R.string.errore) + task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }
}
