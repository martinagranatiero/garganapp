package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class VisualizzazioneCittaActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText descrizione, titolo;
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    private Button btnModificaCitta;
    private Button btnCaricaImmagine;
    private Button btnEliminaCitta;
    private ImageView inputProjectImage;

    String imageUri, downloadImageUrl, title, descr;

    private boolean permessoConcesso = false;
    private static final int PICK_IMAGE = 1;
    private String newPath = "";
    private Uri selectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizzazione_citta);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.dettaglio_citta);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String k = getIntent().getStringExtra("key");
        descrizione = findViewById(R.id.etDescrizioneCitta);
        titolo = findViewById(R.id.txtInputCitta);
        inputProjectImage = findViewById(R.id.imageUpload);
        btnModificaCitta = findViewById(R.id.btnAggiungiCitta);
        btnCaricaImmagine = findViewById(R.id.btnUpload);
        btnEliminaCitta = findViewById(R.id.btnEliminaCitta);

        mStorageRef = FirebaseStorage.getInstance().getReference("city images");
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("cities").child(k);

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    title = dataSnapshot.child("titolo").getValue().toString();
                    descr = dataSnapshot.child("description").getValue().toString();
                    imageUri = dataSnapshot.child("image").getValue().toString();
                    titolo.setText(title);
                    descrizione.setText(descr);
                    Picasso.get().load(imageUri).into(inputProjectImage);
                } else {
                    VisualizzazioneCittaActivity.this.finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });

        //al click del bottone "carica immagine" si richiedono i permessi all'utente
        btnCaricaImmagine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(VisualizzazioneCittaActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!=
                        PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(VisualizzazioneCittaActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            PICK_IMAGE);
                else {
                    permessoConcesso = true;
                }

                if(permessoConcesso) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_IMAGE);
                } else {
                    ActivityCompat.requestPermissions(VisualizzazioneCittaActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            PICK_IMAGE);
                }
            }
        });

        //al click del bottone "elimina città" si riporta l'utente nell'elenco delle città
        // e si rimuove quella città dal database avvisando l'utente dell'avvenuta eliminazione
        btnEliminaCitta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VisualizzazioneCittaActivity.this, ElencoCittaActivity.class));
                mDatabaseRef.removeValue();
                Toast.makeText(VisualizzazioneCittaActivity.this, R.string.eliminazioneCittaSuccesso, Toast.LENGTH_LONG).show();
            }
        });

        //al click del pulsante "salva modifiche" deve aggiornare tutti i dati nel database
        btnModificaCitta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        //controllo se i seguenti campi sono stati modificati o meno
                        //in caso affermativo aggiorna i dati nel database
                        if (!title.equals(titolo)) {
                            dataSnapshot.getRef().child("titolo").setValue(titolo.getText().toString());
                        }

                        if (!descr.equals(descrizione)) {
                            dataSnapshot.getRef().child("description").setValue(descrizione.getText().toString());
                        }

                        if (selectedImage != null) {
                            newPath = selectedImage.getLastPathSegment();

                            //salva nello storage la nuova immagine
                            final StorageReference riversRef = mStorageRef.child(selectedImage.getLastPathSegment()+".jpg");

                            final UploadTask uploadTask = riversRef.putFile(selectedImage);

                            //serve per ottenere il link dell'immagine nello storage per inserirlo
                            //nel campo image della città che si sta modificando
                            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                        @Override
                                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                            if(!task.isSuccessful()) {
                                                throw task.getException();
                                            }

                                            downloadImageUrl = riversRef.getDownloadUrl().toString();
                                            return riversRef.getDownloadUrl();
                                        }
                                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            if(task.isSuccessful()) {
                                                downloadImageUrl = task.getResult().toString();
                                                dataSnapshot.getRef().child("image").setValue(downloadImageUrl);
                                            }
                                        }
                                    });
                                }
                            });
                        }

                        //una volta salvate le modifiche si riporta l'utente nell'elenco delle città
                        // e si avvisa l'utente dell'avvenuta modifica
                        VisualizzazioneCittaActivity.this.finish();
                        Toast.makeText(VisualizzazioneCittaActivity.this, R.string.modificaCittaSuccesso, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    //permessi per accedere alla galleria
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permessoConcesso = true;
        }
    }

    //una volta scelta l'immagine, aggiorna l'immagine visualizzata con quella scelta
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && null != data) {
            if (resultCode == Activity.RESULT_OK) {
                selectedImage = data.getData();
                inputProjectImage.setImageURI(selectedImage);
            }
        }
    }
}
