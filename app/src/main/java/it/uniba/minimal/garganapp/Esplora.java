package it.uniba.minimal.garganapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Esplora extends Fragment {

    private RecyclerView mRecyclerView;
    private ImageAdapter mAdapter;

    private DatabaseReference mDatabaseRef;
    private List<Citta> mCitta;

    public Esplora() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_esplora, container, false);

        mRecyclerView = v.findViewById(R.id.recyclerviewEsplora);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        mCitta = new ArrayList<>();

        //creato riferimento al Realtime Database che contiene le informazioni delle città
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("cities");

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //ciclo per prendere tutte le città presenti nel database
                for(DataSnapshot postShapshot : dataSnapshot.getChildren()) {
                    Citta citta = postShapshot.getValue(Citta.class);
                    mCitta.add(citta);
                }

                //setto l'adapter per la visualizzazione delle card città
                mAdapter = new ImageAdapter(getActivity(), mCitta);
                //se clicco sulla card della città visualizzo i dettagli di tale città
                mAdapter.setOnItemClickListener(new ImageAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        Citta selectedItem = mCitta.get(position);
                        String selectedKey = selectedItem.getCid();
                        Intent intent= new Intent(getActivity(), DettagliCittaActivity.class);
                        intent.putExtra("key",selectedKey);
                        startActivity(intent);
                    }



                });
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }
}
