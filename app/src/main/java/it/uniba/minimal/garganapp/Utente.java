package it.uniba.minimal.garganapp;

public class Utente {
    private String name, email, surname, telefono;

    public Utente() {}

    public Utente(String name, String email, String surname, String telefono) {
        this.name = name;
        this.email = email;
        this.surname = surname;
        this.telefono = telefono;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getSurname() {
        return surname;
    }

    public String getTelefono() {
        return telefono;
    }
}
