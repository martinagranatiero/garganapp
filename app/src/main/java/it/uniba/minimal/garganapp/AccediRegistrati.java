package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AccediRegistrati extends AppCompatActivity {
    EditText nome,cognome,email,password,reinserisciPassword,numerocell,emailAccedi,passwordAccedi;
    TextView forgotPassword;
    Button accedi;
    Button registrati;
    FirebaseAuth fAuth;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore fStore;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accedi_registrati);


        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.toolbar_name_accediregistrati);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //creata una tab per far in modo che l'utente possa inserire le credenziali se già loggato
        //o inserire le sue informazioni personali per registrarsi per la prima volta
        TabHost tabLayout = (TabHost) findViewById(R.id.tab_layout);
        tabLayout.setup();

        TabHost.TabSpec spec=tabLayout.newTabSpec("Tab one");
        spec.setContent(R.id.accedi);
        spec.setIndicator(getResources().getString(R.string.accediButton));
        tabLayout.addTab(spec);

        spec=tabLayout.newTabSpec("Tab Two");
        spec.setContent(R.id.registrati);
        spec.setIndicator(getResources().getString(R.string.registratiBottone));
        tabLayout.addTab(spec);

        nome=findViewById(R.id.nome);
        cognome=findViewById(R.id.cognome);
        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        reinserisciPassword=findViewById(R.id.reinserisciPassword);
        numerocell=findViewById(R.id.telefono);
        registrati=findViewById(R.id.registratiBottone);
        forgotPassword=findViewById(R.id.cambiaPassword);

        fAuth=FirebaseAuth.getInstance();
        fStore=FirebaseFirestore.getInstance();

        emailAccedi=findViewById(R.id.textemail);
        passwordAccedi=findViewById(R.id.textPassword);
        accedi=findViewById(R.id.accediButton);
        firebaseAuth=FirebaseAuth.getInstance();

        //se l'utente è loggato verrà mostrato il profilo al posto dell'activity accedi-registrati
        if(fAuth.getCurrentUser() != null)
        {
            startActivity(new Intent(getApplicationContext(),Profilo.class));
            finish();
        }

        //sezione per reimpostare la password
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //viene creata una finestra di dialogo per chiedere all'utente la volontà di reimpostare la password
                final EditText resetMail=new EditText(v.getContext());
                final AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(v.getContext());
                passwordResetDialog.setTitle(R.string.resetPassword);
                passwordResetDialog.setMessage(R.string.emailPasswordDimenticata);
                passwordResetDialog.setView(resetMail);

                passwordResetDialog.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //si richiede l'email a cui inviare il link per cambiare password
                     String mail=resetMail.getText().toString();
                     firebaseAuth.sendPasswordResetEmail(mail).addOnSuccessListener(new OnSuccessListener<Void>() {
                         @Override
                         public void onSuccess(Void aVoid) {
                             Toast.makeText(AccediRegistrati.this,getResources().getString(R.string.emailPasswordDimenticataInviata),Toast.LENGTH_SHORT).show();
                         }
                     }).addOnFailureListener(new OnFailureListener() {
                         @Override
                         public void onFailure(@NonNull Exception e) {
                             Toast.makeText(AccediRegistrati.this,getResources().getString(R.string.emailPasswordDimenticataRigettata)+e.getMessage() ,Toast.LENGTH_SHORT).show();
                         }
                     });
                    }
                });

                passwordResetDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //chiusura del dialogo
                    }
                });

                passwordResetDialog.create().show();
            }
        });

        //accesso dell'utente all'app
        accedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String varEmail= emailAccedi.getText().toString().trim();
                String varPassword= passwordAccedi.getText().toString().trim();

                //si verifica che il campo email sia stato compilato correttamente
                if(TextUtils.isEmpty(varEmail))
                {
                    emailAccedi.setError(getResources().getString(R.string.emailObbligatoria));
                    return;
                }

                //si verifica che il campo password sia stato compilato correttamente
                if(TextUtils.isEmpty(varPassword))
                {
                    passwordAccedi.setError(getResources().getString(R.string.passwordObbligatoria));
                    return;
                }

                //si verificano che le credenziali inserite siano corrispondenti a quelle presenti su Firebase nella sezione Authentication
                firebaseAuth.signInWithEmailAndPassword(varEmail,varPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                     if(task.isSuccessful())
                     {
                         Toast.makeText(AccediRegistrati.this,getResources().getString(R.string.accessoEffettuato),Toast.LENGTH_SHORT).show();
                         startActivity(new Intent(getApplicationContext(),MainActivity.class));
                         finish();
                     }
                     else
                     {
                         Toast.makeText(AccediRegistrati.this,getResources().getString(R.string.errore) + task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                     }
                    }
                });
            }
        });

        //registrazione dell'utente all'app
        registrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String varEmail= email.getText().toString().trim();
                final String varPassword= password.getText().toString().trim();
                String varReinserisciPassword=reinserisciPassword.getText().toString().trim();

                 final String varNome=nome.getText().toString();
                 final String varCognome=cognome.getText().toString();
                 final String varTelefono=numerocell.getText().toString();

                //controllo che email e password siano inseriti
                if(TextUtils.isEmpty(varEmail))
                {
                    email.setError(getResources().getString(R.string.emailObbligatoria));
                    return;
                }

                if(TextUtils.isEmpty(varPassword))
                {
                    password.setError(getResources().getString(R.string.passwordObbligatoria));
                    return;
                }
                //si verifica che la password corrisponda con quella inserita al momento della registrazione
                if(varPassword.compareTo(varReinserisciPassword) != 0)
                {
                    reinserisciPassword.setError(getResources().getString(R.string.passwordErrata));
                    return;
                }

                fAuth.createUserWithEmailAndPassword(varEmail,varPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {

                            // inviata email di verificazione account
                            FirebaseUser fuser=fAuth.getCurrentUser();
                            fuser.sendEmailVerification().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(AccediRegistrati.this,getResources().getString(R.string.emailVerificaInviata),Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("TAG",getResources().getString(R.string.emailVerificaRigettata)+ e.getMessage());
                                }
                            });

                            Toast.makeText(AccediRegistrati.this,getResources().getString(R.string.utenteCreato),Toast.LENGTH_SHORT).show();
                            userID=fAuth.getCurrentUser().getUid();
                            DocumentReference documentReference=fStore.collection("users").document(userID);
                            //vengono salvate le informazioni al momento della registrazione nel db (nome,cognome,email,password,telefono)
                            Map<String,Object> user= new HashMap<>();
                            user.put("name",varNome);
                            user.put("surname",varCognome);
                            user.put("telefono",varTelefono);
                            user.put("email",varEmail);
                            documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d("TAG",getResources().getString(R.string.registrazioneSuccesso) + userID);
                                }

                                }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("TAG",getResources().getString(R.string.registrazioneFallita)+ e.toString());
                                }
                                }) ;
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        }
                        else
                        {
                            Toast.makeText(AccediRegistrati.this,getResources().getString(R.string.errore) + task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });


    }
}
