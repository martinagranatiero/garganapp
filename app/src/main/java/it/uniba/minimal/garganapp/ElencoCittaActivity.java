package it.uniba.minimal.garganapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ElencoCittaActivity extends AppCompatActivity implements ImageAdapter.onItemClickListener {

    Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private ImageAdapter mAdapter;

    private DatabaseReference mDatabaseRef;
    private List<Citta> mCitta;

    SharedPreferences preferences;
    public static final String PREFS_NAME="Garganapp_shared_pref";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elenco_citta);

        //per accedere a un'istanza della classe SharedPreferences associata ad un nome e
        //ad un identificatore dei permessi di accesso da parte delle altre applicazioni
        preferences = this.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        //Utile per inserire la toolbar all'interno dell'activity
        toolbar = (Toolbar) findViewById(R.id.toolbar_elenco_citta);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title_elenco_citta);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.toolbar_name_elencocitta);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //serve per tornare nell'activity precedente (controllare anche il manifest)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView= findViewById(R.id.recyclerviewCittaStaff);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        mCitta=new ArrayList<>();
        mDatabaseRef= FirebaseDatabase.getInstance().getReference("cities");

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mCitta.clear();
              for(DataSnapshot postShapshot : dataSnapshot.getChildren())
              {
                  Citta citta = postShapshot.getValue(Citta.class);
                  mCitta.add(citta);
              }
                ottieniImpostazioni();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ElencoCittaActivity.this, databaseError.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        Citta selectedItem=mCitta.get(position);
        String selectedKey=selectedItem.getCid();
        Intent intent= new Intent(this,VisualizzazioneCittaActivity.class);
        intent.putExtra("key",selectedKey);
        startActivity(intent);
    }

    //per specificare le opzioni del menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_elenco_citta, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Questo metodo passa l'opzione selezionata. È possibile identificare l'elemento attraverso l'ID che è univoco
    //È possibile abbinare questo ID a voci di menu note per eseguire l'azione appropriata.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        SharedPreferences.Editor editor;
        if (id == R.id.ascendente) {
            editor= preferences.edit();
            editor.putString(getString(R.string.sort),getString(R.string.ascendente));
            editor.apply();
            ottieniImpostazioni();

        }
            if (id == R.id.discendente) {
                editor= preferences.edit();
                editor.putString(getString(R.string.sort),getString(R.string.discendente));
                editor.apply();
                ottieniImpostazioni();
            }

        return super.onOptionsItemSelected(item);
    }
    //per ottenere le impostazioni di ordinamento dalle shared preferences
    private void ottieniImpostazioni(){
        String sortSettings= preferences.getString(getString(R.string.sort),getString(R.string.ascendente));
        if(sortSettings.equals(getString(R.string.ascendente))) {
            Collections.sort(mCitta, Citta.SORT_BY_ASCENDING);
        }else if(sortSettings.equals(getString(R.string.discendente))){
            Collections.sort(mCitta,Citta.SORT_BY_DESCENDING);
        }
        mAdapter=new ImageAdapter(ElencoCittaActivity.this, mCitta);
        mAdapter.setOnItemClickListener(ElencoCittaActivity.this);
        mRecyclerView.setAdapter(mAdapter);
    }

}
